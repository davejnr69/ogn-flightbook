## Production setup
```
TODO
```

## Development set up
```
TODO
```

## Architecture in depth
Redis is the central piece of architecture.  
Data structures used are List, Geozset & Stream. If you are new to Redis, you can read following docs:
 
 - [Redis data-types](https://redis.io/topics/data-types)
 - [Redis Geocoding](https://redis.io/commands/geoadd)
 - [Redis Stream](https://redis.io/topics/streams-intro)


### Redis as a Queue Manager
![Diag1](./pics/Diag1.png)
### Redis for Geocoding
#### Find closest Airfield:
![Diag2](./pics/Diag2.png)
#### Find surrounding beacons:
![Diag3](./pics/Diag3.png)
### Redis for streaming Data

## Scalability in mind
```
TODO
```

### Customize configuration
```
TODO
```
