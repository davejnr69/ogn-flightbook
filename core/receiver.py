import json
from datetime import timezone
import logging

from ogn.parser import parse, ParseError

from .aprs import AClient


def process_beacon(raw_message, red, check):
    try:
        # in ogn parser, if reference_timestamp is not defined => reference_timestamp = datetime.utcnow()
        # pb if server tz is not 0.
        # the reference_timestamp should be: datetime.now(timezone.utc)
        # ie: beacon = parse(raw_message, reference_timestamp=datetime.now(timezone.utc))
        # https://docs.python.org/fr/3/library/datetime.html#datetime.datetime.fromtimestamp
        # but the parse.py and crate timestamp is buggy.
        # Issue reported and correction made see https://github.com/glidernet/python-ogn-client/issues/84
        # & https://github.com/glidernet/python-ogn-client/issues/85
        # wait for python-ogn-client release
        beacon = parse(raw_message)
        if beacon['aprs_type'] == 'position' and beacon['beacon_type'] in ['aprs_aircraft', 'flarm', 'tracker']:
            fix = dict()
            address = beacon['address']
            # TODO wait for python-ogn-client release
            # fix['tsp'] = int(datetime.timestamp(beacon['timestamp']))
            fix['tsp'] = int(beacon['timestamp'].replace(tzinfo=timezone.utc).timestamp())

            # multi-receivers case (height receiver density)=> we have to sort aircraft messages by timestamp
            # messages could be unordered when process, furthermore we can have 2 same records
            old_tsp = check.get(address, 0)
            if fix['tsp'] <= old_tsp:
                return
            check[address] = fix['tsp']

            fix['alt'] = round(beacon['altitude']) if beacon['altitude'] is not None else None
            fix['lat'] = beacon['latitude']
            # lat is limited for areas very near to the poles
            # read: https://redis.io/commands/geoadd
            # exclude beacon message in this case
            if abs(beacon['latitude']) > 85.05:
                return
            fix['lng'] = beacon['longitude']
            fix['clr'] = round(beacon['climb_rate'], 2) if beacon['climb_rate'] is not None else None
            fix['gsp'] = round(beacon['ground_speed']) if beacon['ground_speed'] is not None else None
            fix['track'] = beacon['track']
            fix['steal'] = beacon.get('stealth', False)

            # use redis pipeline for speed-up
            p = red.pipeline(transaction=False)     # python pipeline implementation use transactional by default
            p.rpush('BA:{}'.format(address), json.dumps(fix))
            p.geoadd('GEO:A', beacon['longitude'], beacon['latitude'], address)
            p.sadd('SQBA', 'BA:{}'.format(address))
            p.execute()
    except ParseError:
        pass


def run(red):
    client = AClient(aprs_user='N0CALL', redis=red)
    client.connect()
    while True:
        try:
            client.run(callback=process_beacon, autoreconnect=True)
        except KeyboardInterrupt:
            print('\nStop ogn gateway')
            client.disconnect()
            break
        except BaseException as e:
            logging.warning(repr(e))
