# SETTING configuration
# DON'T USE ANY SECRET IN THIS FILE BUT USE setting_local.py INSTEAD

# REDIS SETTING
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 0

# SQLITE SETTING
# if PATH is None, the sqlite file will be created on project top directory
SQLITE_PATH = None

# PRIVACY MAPPING SALT
PRIVACY_SALT = "3v3ry 5tr1ng u w@nt h3r3"

# Try to override with setting_local if any
try:
    from setting_local import *
except ImportError:
    pass
